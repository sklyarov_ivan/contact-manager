<?php

abstract class Model
{
  
    public static function FactoryModel($class)
    {
        return new $class;
    }
    public function findElementById($id)
    {
        $object = Database::getInstance();
        $object->doQuery('SELECT id, email, password, create_time FROM '.$this->_table.' WHERE id='.$id);
        return $object->loadObjectList();
    }
    
    public function deleteElementById($id,$user_id='')
    {
        $object = Database::getInstance();
        $object->doQuery('DELETE FROM '.$this->_table.' WHERE id='.$id);
        return true;
    }
    
    public function updateElementById($id, $arrayVariables)
    {
        if (is_array($arrayVariables) && !empty($arrayVariables)) {
            $object = Database::getInstance();
            $newElements = '';
            foreach ($arrayVariables as $key=>$value) {
                $newElements = ''.$key.'='.$value.', ';
            }
            $newElements = substr($newElements, 0, strlen($newElements)-2);
            $object->doQuery('UPDATE '.$this->_table.' SET '.$newElements.' WHERE id="'.$id.'"');
            return $object->loadObjectList();
        }
        return true;
    }
    
    public function setElementData()
    {
        $dataOdject = get_object_vars($this);
        $classname = get_class($this);
        $reflection = new ReflectionClass($classname);
        $reflectionProperty = $reflection->getProperty('_table');
        $reflectionProperty->setAccessible(true);
        $table = $reflectionProperty->getValue(new $classname);
        unset($dataOdject['id']);
        unset($dataOdject['_table']);
        foreach($dataOdject as $key=>$val)
        {
            $dataOdject[$key]="'$val'";
        }
        $keys = implode(', ', array_keys($dataOdject));
        $values = implode(', ', array_values($dataOdject));
        $object = Database::getInstance();
        $object->doQuery('INSERT INTO '.$table.'('.$keys.') VALUES('.$values.')');
        return true;
        
    }
    
    public function applyElements($elementsArray)
    {
        $classname = get_class($this);
        $reflection = new ReflectionObject(new $classname);
        
        foreach ($elementsArray as $key=>$val)
        {
            if ($reflection->hasProperty($key)){
                $this->$key = $val;
            }
            
        }
        return $this;
    }
}