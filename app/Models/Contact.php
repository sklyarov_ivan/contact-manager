<?php

class Contact extends Model
{
    public $id;
    public $fio;
    public $birth_date;
    public $phone_number;
    public $email;
    public $users_id;
    private $_table = 'contacts';
    
    public function getContactListByUserId($id)
    {
        $object = Database::getInstance();
        $object->doQuery('SELECT * FROM '.$this->_table.' WHERE users_id = "'.$id.'" ORDER BY id DESC');
        return $object->loadList();
    }
     public function deleteElementById($id, $user_id='')
    {
        $object = Database::getInstance();
        $object->doQuery('DELETE FROM '.$this->_table.' WHERE id="'.$id.'" AND users_id = "'.$user_id.'"');
        return true;
    }
}