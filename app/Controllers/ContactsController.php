<?php
class ContactsController extends Controller
{
    private $response;
    public function __construct()
    {
        parent::__construct();
        $this->response = array();
    }
    public function index()
    {
        return $this->redirect();
    }
    
   public function addContact()
    {
        if ($this->isAjax())
        {
            foreach($_POST as $post_key=>$post_val)
            {
                $_POST[$post_key] = stripcslashes($post_val);
            }
            $contact = Model::FactoryModel('contact');
            if (isset($_SESSION['user']['id'])) {
                $_POST['users_id'] = $_SESSION['user']['id'];
                $newContact = $contact->applyElements($_POST);
                if($newContact->setElementData()) {
                    $this->response['status'] = 'OK';
                    $this->response['action'] = 'set';
                } else {
                    $this->response['status'] = 'NO';
                }
                
            }
            echo json_encode($this->response);
        }else{
            return $this->redirect();
        }
        
    }
    
    public function getContacts()
    {
        if ($this->isAjax())
        {
            if (isset($_SESSION['user']['id'])) {
                $contact = Model::FactoryModel('contact')->getContactListByUserId($_SESSION['user']['id']);
                $this->response['contacts'] = $contact;
                $this->response['action'] = 'get';
                $this->response['status'] = 'OK';
            } else {
                $this->response['status'] = 'NO';
            }
            echo json_encode($this->response);

        }else{
            return $this->redirect();
        }
    }
     public function deleteContact()
    {
        if ($this->isAjax())
        {
            $_POST['id']=  stripcslashes($_POST['id']);
            $contact = Model::FactoryModel('contact');
            if (isset($_SESSION['user']['id'])) {
                $_POST['users_id'] = $_SESSION['user']['id'];
                if($contact->deleteElementById($_POST['id'],$_POST['users_id'])) {
                    $this->response['contacts'] = $contact->getContactListByUserId($_SESSION['user']['id']);
                    $this->response['status'] = 'OK';
                    $this->response['action'] = 'del';
                } else {
                    $this->response['status'] = 'NO';
                }
                
            }
            echo json_encode($this->response);
        }else{
            return $this->redirect();
        }
        
    }
}