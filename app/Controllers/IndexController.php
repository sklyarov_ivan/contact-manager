<?php
//namespace app\Controllers;
class IndexController extends Controller
{
    private $response;
    function __construct()
    {
        parent::__construct();
        $this->response = array();
    }
    function index()
    {
        $view = new TemplateRender();
        $view->render('content');
    }
    
    function registrate()
    {
        if ($this->isAjax())
        {
            if(isset($_POST['email']) && isset($_POST['password']) &&
                    !empty($_POST['password'])&& !empty($_POST['email'])){
                $this->response['email'] = $_POST['email'];
                $email = stripcslashes($_POST['email']);
                $user = Model::FactoryModel('user');
                $result = $user->getUserByEmail($email);
                if (!is_object($result)) {
                    $user->email = $email;
                    $user->password = $_POST['password'];
                    $user->create_time = time();
                    if($user->setElementData())
                    {
                        $_SESSION['user']['id'] = Model::FactoryModel('user')->getUserByEmail($email)->id;
                        $this->response['user_data'] = Model::FactoryModel('user')->getUserByEmail($email);
                        $this->response['status'] = 'OK';
                    }
                } else {
                    if($result->password === $_POST['password']) {
                        $_SESSION['user']['id'] = $result->id;
                        $this->response['user_data'] = $result;
                        $this->response['status'] = 'OK';
                    } else
                        $this->response['status'] = 'NO';
                }
            }else {
                $this->response['status'] = 'NO';
            }
                echo json_encode($this->response);
        }else{
            return $this->redirect();
        }
    }
}