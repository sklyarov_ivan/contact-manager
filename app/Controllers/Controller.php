<?php
//namespace app\Controllers;

abstract class Controller
{
    public function __construct()
    {
        
    }
    
    abstract public function index();
    
    public function isAjax ()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest") 
                return true;
        return false;
    }
    
    public function redirect()
    {
        return header('Location: '.SITE_URL);
    }
    
}
