<!DOCTYPE html>
<html>
  <head>
    <title><?php isset($title)?$title:''; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo SITE_URL?>css/main.css" rel="stylesheet" media="screen">
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="<?php echo SITE_URL?>bootstrap/js/bootstrap.min.js"></script>
    <script src="http://underscorejs.org/underscore-min.js"></script>
    <script src="http://backbonejs.org/backbone-min.js"></script>
    <script src="<?php echo SITE_URL?>js/md5.js"></script>
    <script src="<?php echo SITE_URL?>js/watch.js"></script>
    <script src="<?php echo SITE_URL?>js/main.js"></script>
  </head>
  <body>
    <?php echo $content; ?>
  </body>
</html>