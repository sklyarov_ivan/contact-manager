<?php

class Database
{
   private $host = DB_HOST;
   private $user = DB_USER;
   private $pass = DB_PASSWORD;
   private $dbName = DB_NAME;
    
   private static $instance;
    
   private $connection;
   private $results;
   private $numRows; 
    
   private function __construct()
   {      
       $this->connect();
   }
    
   static function getInstance()
   {
      if(!self::$instance)
      {
         self::$instance = new self();
      }
      return self::$instance;
   }
    
   function connect()
   {
      $this->connection = mysqli_connect($this->host,
                                         $this->user,
                                         $this->pass,
                                         $this->dbName);
   }
    
   public function doQuery($sql)
   {      
      $this->results = mysqli_query($this->connection, $sql);
      if ($this->results)
        return true;
      else
        return false;
   }
    
   public function loadObjectList()
   {
      $obj = "No Results";
      if ($this->results)
      {
         $obj = mysqli_fetch_object($this->results);
      }
      return $obj;
   }
   public function loadList()
   {
      $obj = "No Results";
      if ($this->results)
      {
          $obj = array();
          while($row = mysqli_fetch_object($this->results))
          {
              $obj[] = $row;
          }
      }
      return $obj;
   }
   
   public function lastId()
   {
       if($this->results)
       {
           $this->results->insurt_id;
       } else {
           return false;
       }
   }
} 