$(document).ready(function(){
    var BASE_URL = window.location.host+'/contact-manager';
    var contacts = {group:''};

    var SignInUp = Backbone.View.extend({
        el: $('.signin_up'),
        events: {
            'click #sign_up_in':'checkForm',
            'enter-system-ok':'enterSystem',
            'enter-system-no':'enterFail',
        },
        initialize: function(){
        },
        checkForm: function() {
            this.model.set('email',$('input[type=email]').val());
            var password = $('input[type=password]').val();
            this.model.set('password',password);
            if (this.model.isValid()) {
                this.model.set('password',MD5(password));
                this.model.save();
                $('#login_error').remove();
            } else
                this.checkFields(this.model.validationError)

        },
        enterSystem: function(){
            $(this.el).hide('slow');
            $('.contact-book').trigger('show-system');
        },
        enterFail: function(){
            this.checkFields('User with this email already redistrated in system. Enter another email or check password');
        },
        checkFields:function(error){
             if($('legend').next().attr('class').match(/alert/) === null) {
                    $('legend').after('<div class="alert alert-error" id="login_error">'+error+'</div>')
                }else{
                    $(this.el).find('#login_error').html(error);
                }
        }
    });
    var User = Backbone.Model.extend({
        defaults: {
            email:'',
            password:''
        },
        attributes: function(){
            var attr = {
                email:this.get('email'),
                password:this.get('password')
            }
            return attr;
        },
        save: function(){
            $.ajax({
                url: 'http://'+BASE_URL+'/index/registrate',
                type: 'POST',
                data: this.attributes,
                dataType: 'json',
                success: function(response){
                    if(typeof(response.status) !== 'undefined' && response.status === "OK") {
                        $('.signin_up').trigger('enter-system-ok')
                    } else {
                        $('.signin_up').trigger('enter-system-no')
                    }
                },
                error: function(a,b,c) {
                    console.log(a+' | '+b+' | '+c);
                }
            });
        },
        validate: function(attrs, options) {
            if (!isValidEmailAddress(attrs.email))
                return "Invalid Email";
            if (attrs.password.length < 5)
                return 'Passwods lendth must me 5 of more simbols';
        }
    })
    var Contact = Backbone.Model.extend({
        defaults: {
            id:'',
            fio:'',
            birth_date:'',
            phone:'',
            email:'',
            phone_number:''
        },
        attributes: function(){
            var attr = {
                id:this.get('id'),
                email:this.get('email'),
                birth_date:this.get('birth_date'),
                phone_number:this.get('phone_number'),
                fio:this.get('fio')
            }
            return attr;
        },
        save: function($action){
            $.ajax({
                url: 'http://'+BASE_URL+'/contacts/'+$action,
                type: 'POST',
                data: this.attributes,
                dataType: 'json',
                success: function(response){
                    if (typeof(response.status) !== 'undefined' && response.status === "OK") {
                        if (response.action === 'set')
                            $('.contact-book').trigger('showContactList');
                        if (response.action === 'get' || response.action === 'del'){
                            $('.contact-book').trigger('buildList');
                            contacts.group = response.contacts;
                        }
                    }
                },
                error: function(a,b,c) {
                    console.log(a+' | '+b+' | '+c);
                }
            });
        },
        validate: function(attrs, option) {
            if (!isValidEmailAddress(attrs.email))
                return "Invalid Email";
            if (attrs.phone_number.match(/\d{11}/) === null)
                return 'Phone may have only 11 numeric simbols';
            if (attrs.fio.match(/[A-Za-z]{3,25}/) === null)
                return 'FIO must have only russian or englich language simbols and simbols count between (3,25)';
            if (attrs.birth_date.match(/\d{1,2}\/\d{1,2}\/\d{2,4}/) === null)
                return 'Birth date must have format dd/mm/yyyy';


        }
    });
    var ContactBook = Backbone.View.extend({
        el: $('.contact-book'),
        events: {
            'show-system': 'showSystem',
            'click #addContact':'addContact',
            'showContactList':'showContactList',
            'buildList':'buildList',
            'click #deleteContact':'deleteContact',
            'invalid':'checkFields'
        },
        showSystem: function(){
            $(this.el).show('slow');
            this.showContactList();
        },
        addContact: function(){
        var self = this;
                
                $(this.el).find('input[type=text]').each(function(key,val){
                    self.model.set($(val).attr('name'),$(val).val())
                });
                if(self.model.isValid()) {
                     $(this.el).find('input[type=text]').each(function(key,val){
                        $(val).val('');
                    });
                    $(this.el).find('.alert').remove();
                    this.model.save('addContact');
                } else
                    this.checkFields(self.model.validationError);
        },
        checkFields: function(error){
        var inputBox = $(this.el).find('input[type=text]').parent();
                if(inputBox.next().attr('class').match(/alert/) === null) {
                    inputBox.after('<div class="alert alert-error" style="margin-top:20px;">'+error+'</div>')
                }else{
                    $(this.el).find('.alert').html(error);
                }
            
        },
        deleteContact:function(el){
            var $id = $(el.currentTarget).parent().parent().find('input[type=hidden]').val();
            this.model.set('id',$id);
            this.model.save('deleteContact');
        },
        showContactList: function() {
            this.model.save('getContacts');
        },
        buildList: function() {
            $('.contactList').html('');
            contacts.watch("group", function (id, oldval, newval) {
                for (var i in newval){
                $('.contactList').prepend( 
                                    '<tr class="contact">'+
                                            '<td id="fio">'+newval[i].fio+'</td>'+
                                            '<td id="birth_date">'+newval[i].birth_date+'</td>'+
                                            '<td id="phone">'+newval[i].phone_number+'</td>'+
                                            '<td id="email">'+newval[i].email+'</td>'+
                                            '<td><input type="hidden" value="'+newval[i].id+'"></td>'+
                                            '<td id="delete">'+
                                            '<a class="btn btn-danger" id="deleteContact">Delete</a>'+
                                            '</td>'+
                                            '</tr>')
                    
                }
            });
        }
    });
    var contacts = new Contact();
    var system = new ContactBook({model:contacts});
    var user = new User();
    var sign_in_up = new SignInUp({model:user});
});


function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

