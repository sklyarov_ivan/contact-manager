<?php
require_once 'config.php';
define('ROOT',__DIR__);
define('LIB_DIR',ROOT.'/app/lib/');
define('CONTROLLER_DIR',ROOT.'/app/Controllers/');
define('MODEL_DIR',ROOT.'/app/Models/');
define('VIEW_DIR',ROOT.'/app/Views/');
session_start();

set_include_path(get_include_path()
                                    .PATH_SEPARATOR.LIB_DIR
                                    .PATH_SEPARATOR.CONTROLLER_DIR
                                    .PATH_SEPARATOR.MODEL_DIR
                                    .PATH_SEPARATOR.VIEW_DIR);
spl_autoload_register('autoload');
function autoload($className)
{    
        $filename = $className.'.php';
        $file = stream_resolve_include_path($filename);
        if($file !== FALSE)
            require_once $filename;
}

$router = new FrontController();
$router->run();